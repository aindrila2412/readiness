# Dependency Proxy on GitLab.com

With the change to puma, we can now enable the dependency proxy. This is a service that caches docker images to improve CI job times.

## Summary
Many projects depend on a growing number of images and dependencies that must be fetched from external sources with each build. This slows down build times and introduces availability issues into the supply chain. In addition, many of these external sources come from unknown and unverified providers, introducing potential security vulnerabilities.

The Dependency Proxy will help users by allowing them to proxy and cache those images and dependencies, so they can reduce their reliance on external libraries and minimize repetitive downloads. 

The MVC of the Dependency Proxy launched in milestone 11.11. However, it has gone mostly unused due to a requirement to use [Puma as the web seververs](https://docs.gitlab.com/omnibus/settings/puma.html) and not [Unicorn](https://docs.gitlab.com/ee/development/architecture.html#unicorn) which has been in use for gitlab.com and the preferred setting for self-managed instances.

### Target audience and experience
​​This feature impacts five types of users:

- Software Developer: As a developer, you don’t want to spend time worrying about external packages. You want fast build times and a seamless deployment process. The dependency proxy will allow you to store packages for faster and more reliable access. We will start with Docker, our most popular package management tool and expand to include other tools such as NPM and Maven. In the future, we will provide more transparency to help troubleshoot build issues. You will be able to view build logs, track diffs and easily search through the entire proxy.
- Security Analyst: As a security analyst, you need visibility into which external dependencies are being introduced into the supply chain and by whom. The dependency proxy will provide that visibility, but also will grant you the ability to set policies and create lists of approved and banned packages. We know you work on more than one project, so we will give you the tools and ability to discover and navigate all of the proxy repositories at an organizational level. We’ll start with the command line, add support for APIs, and evaluate adding a centralized dashboard.
- QA Engineer: As a QA engineer, you need the ability to work across multiple projects and teams seamlessly. The goal of the dependency proxy will be to ensure that each codebase you test not only runs on your machine (or environment) but that you can start testing quickly and get back to automation.
- Systems Administrator​​: As a systems administrator, you need to know that your entire DevOps pipeline runs smoothly and efficiently. For those of you at organizations that limit internet access, the dependency proxy will allow you to control the chaos and manage a single, optimized storage system for all your external packages. We will provide deduplication, garbage collection and the ability to set your own policies to help optimize storage and save money.

### Use cases
1. Provide a single method of reaching upstream package management utilities, in the event they are not otherwise reachable. This is commonly due to network restrictions.
1. Let proxy packages act as a cache for increased pipeline build speeds.
1. Verify package integrity from one single place. See what has been changed and test them for security vulnerabilities (part of black duck model).
1. Filter the available upstream packages to include only approved, whitelisted packages.
1. Track which dependencies are utilized by which projects when pulled through the proxy. (Perhaps when authenticated with a CI_JOB_TOKEN.
1. Audit logs in order to find out exactly what happened and with what code.
1. Operate when fully cut off from the internet with local dependencies.
1. Enforce policies at the proxy layer (e.g. scan packages for licenses and only allow packages with compatible licenses).

### Metrics

#### Business Metrics
Since we are just launching this  feature, we will want to track general adoption of the feature right from the start. To do this we can look at:
- Number of self-managed instances with the dependency proxy enabled
- Count of page views to /dependency_proxy
- Cache hit ratio (number of images pulled using the dependency proxy / total number of images pulled)
- To start we should also pay attention to the number of issues being opened and what they are related to. Are we solving a problem or are we missing something?

In addition to customer adoption, we could also look at our own internal adoption of the Dependency Proxy. 
- Any images that are currently being pulled from Docker Hub for the purpose of building and maintaining GitLab should be cached via the Dependency Proxy. 
  - Count of total images used / count of total images cached
- For use cases that we do not currently satisfy with the MVC, we should open new issues to ensure that we are building a feature that solves GitLab's needs.

#### Performance Metrics
- Compare the original download time for a cached image vs how long it takes us to serve it from cache in the future

## Architecture

### Component Description
- The proxy is essentially a localized cache of docker images that are held to allow for faster docker performance in CI. The major component will be the storage mechanism (object storage for GitLab.com), GitLab receives a request from the docker client to download an image, and it first downloads it into object storage, then returning the image in the response. On subsequent requests, it will only have to fetch the data from object storage rather than making external calls.

### Blast radius 
- If the dependency proxy fails for some reason (loss of storage or connection), it will affect any CI users utilizing this feature. It can be turned off to return to the standard behavior of pulling images directly from external sources.

### Single points of failure
- The single point of failure will be in the storage of the proxied images (blobs and manifests). If that storage connection is lost, the dependency proxy will return errors.

## Operational Risk Assessment

### Scalability & Performance Risks
- The only major risk is in CI image pulls. If the dependency proxy gets overloaded and slows down, the image pulls could potentially slow down. This is why we require Puma to allow for more workers. The next step to promote higher scalability and performance is to move the image pull/download logic to workhorse as specified in #11548

### Dependencies and impact of failures
- **Registry storage:** if the connection to object storage fails, then the dependency proxy will not function and when CI tries to pull images, they will error out.
- **Gitlab Rails:** This feature utilizes rails routes and controllers, if the main rails app is not serving requests, it will fail.

### Compromises / cut features
- None apparent, this feature has not changed since it was originally released.

### Top Operational Risks
- Only one. I write a script to pull a lot of images through dependency proxy and response time for other web requests is increased. With Puma we can simply spawn a lot of workers so we don't care if part of them are stuck while docker images are downloaded from docker hub and cached in GitLab before being served to docker client.

### Future Operational Risks
- At launch, no data will exist within the proxy, but overtime, the dependency proxy will build up a large amount of docker images (not unlike the container registry), which could lead to excess disk usage.

### Rollback / Feature Flags
- It can be safely turned off via config settings, but not with a feature flag. It is completely safe to turn off the dependency proxy anytime while in use. The only affect will be pipelines that were previously sped up by the proxy, will return to normal speeds.

### Customer Interactions / Failures
- Customers will be able to enable the dependency proxy at the group level. It will then effectively cache the images they use in CI to speed up subsequent CI runs (no longer having to pull from external APIs). In case of an extreme failure (connection to object storage is lost), their pipelines may fail, however this can be remedied by turning the dependency proxy off.

### Worst-case Scenarios
- Use of Puma could cause complete backup of workers. A solution would possibly be to restrict some web machines to not serve dependency proxy requests.

## Security

### Built following [GitLab security development guidelines](https://about.gitlab.com/security/#gitlab-development-guidelines)
- Not 100% positive, but this is a fairly confident "Yes".

### PII Risk
- No, in it's current state it only will proxy requests and store images from the public docker registry.

## Performance

### GitLab [Performance Guidelines](https://docs.gitlab.com/ce/development/performance.html)
- [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
- [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
- [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)

### Scaling for GitLab.com
- There are no likely scaling concerns for GitLab.com. (This does not touch the database)

### Throttling
- There are no throttling limits imposed by this feature

### Traffic Spikes
- This feature does not account for brief spikes in traffic. We never measured what can be the impact once users start to use it. I don't think we reach 2x though.

## Backup and Restore

This is currently only a caching service and does not need backups.

## Monitoring and Alerts

### Logging
- Logging is in the Rails logs
- Service metrics are not in Promethues

### Measuring Customer Experience 
- Cache hit ratio (number of images pulled using the dependency proxy / total number of images pulled)
- To start we should also pay attention to the number of issues being opened and what they are related to. Are we solving a problem or are we missing something?

### SLA
- We currently don't have a target SLA
- We do not currently have SLI indicators to track the SLA
- We do not currently have alerts that are triggered when the SLI's (and thus the SLA) are not met?
- We do not currently have troubleshooting runbooks
- There are no thresholds for tweeting or issuing customer notifications for outages.   

## Responsibility
- The ~backend Package Team are responsible for this feature with #s_package being the Slack point of contact, and Dan Croft being the engineering manager point of contact with the backend engineers being the subject matter experts. The MVC (as it currently exists) was coded by Dmitriy Zaporozhets, as such he would be the premier expert initially.
- The infrastructure team is responsible for the config, which consists of object storage setup and the `gitlab.rb` config that turns the feature on and off for the entire instance. The package team is responsible for the reliability of the functionality of the feature.
- Someone from the Package Team will be available to be on call, however, Dmitriy Zaporozhets built the feature and would likely be the fastest to answer any major concerns initially.

## Testing
- There was no load test plan for this feature
- There were was no testing for theorized component failures
- The feature is supported by rspec unit (models/services/routes) and integration (controllers) tests.
